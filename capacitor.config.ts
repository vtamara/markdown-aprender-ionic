import type { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'markup-aprender-ionic',
  webDir: 'dist'
};

export default config;
